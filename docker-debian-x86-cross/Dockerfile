FROM debian:bullseye

LABEL maintainer="Nikos Mavrogiannopoulos <nmav@gnutls.org>"

ARG ARCHES="i386"

ARG TRIPLES="i686-linux-gnu"

# not all dev packages seem to be multiarch-ready yet but these are
ARG GNUTLS_DEPS="nettle-dev libp11-kit-dev libtasn1-dev \
	libunbound-dev libssl-dev libcmocka-dev libunistring-dev \
	libopts25-dev libltdl-dev libidn2-dev \
	zlib1g-dev libbrotli-dev libzstd-dev"

ARG GNUTLS_ARCH_ONLY_DEPS="datefudge"

# configure additional arches in dpkg/apt
RUN for arch in ${ARCHES} ; do \
	dpkg --add-architecture $arch ; \
done

RUN apt-get update -qq -y
RUN apt-get dist-upgrade -y
RUN apt-get autoremove -y
RUN apt-get install -y dash git-core autoconf libtool gettext autopoint \
	dpkg-dev automake autogen libtspi-dev guile-2.2-dev gawk gperf \
	dns-root-data bison help2man valgrind nodejs \
	softhsm2 lcov dieharder openssl:i386 socat \
	net-tools ppp lockfile-progs ccache rsync wget python3 ${GNUTLS_DEPS} \
	$(for triple in ${TRIPLES} ; do echo g++-$triple ; done) \
	$(for dep in ${GNUTLS_DEPS} ${GNUTLS_ARCH_ONLY_DEPS}; do \
		for arch in ${ARCHES} ; do echo $dep:$arch ; done ; \
	done)

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# tlsfuzzer deps
RUN apt-get install -y python3-six

RUN mkdir -p /usr/local/
RUN git clone https://gitlab.com/libidn/gnulib-mirror.git /usr/local/gnulib
ENV GNULIB_SRCDIR /usr/local/gnulib
ENV GNULIB_TOOL /usr/local/gnulib/gnulib-tool

CMD ["/bin/dash"]
